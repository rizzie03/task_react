import logo from './logo.svg';
// import './App.css';
import Content from './Components/Contents/Contents';

function App() {
  return (
    <div>
      <Content />
    </div>
  );
}

export default App;