import React from 'react';
import Card from '../Cards/Card';
import './Contents.css';

class Content extends React.Component {
  constructor() {
    super()
    this.state = {
      name : "Destin",
      address : "Jakarta"
    };
  }

  changeName = () => {
    this.setState({ name : "Tintin"});
  }

  changeAddress = () => {
    this.setState({ address : "Apartement"});
  }

  render() {
    return (
      <div className="container">
        <p className="p-container">Name : <Card name = {this.state.name}/></p>
        <p className="p1-container">Address : <Card address = {this.state.address}/></p>
        <button className = "btn-name" onClick={this.changeName}>
          Press to see magic!
        </button>
        <button className = "btn-adr" onClick={this.changeAddress}>
          Press to see another magic!
        </button>
      </div>
    )
  };
}

export default Content;